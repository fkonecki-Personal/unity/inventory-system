# Inventory System

<img src="/Screenshots/Main.png" alt="Screenshot" width="900" height="450">

Exploring:
- Player Movement
- Picking up items
- Inventory, Equipment and Attibute screens
- Tooltip (info) and item split screens
- Opening/closing screens
- Flexible inventory grid screen
- Dragging, Using, Equipping, Unequipping, Dropping and Splitting items
- Different types of items
- Camera follow delay
- Cinematics
- Item durability
- Mobile touch input
- SafeArea
- Coroutines
- Analytics
