﻿using UnityEngine;
using UnityEngine.Events;

namespace InventorySystem
{
    public class GameEventListener : MonoBehaviour
    {
        [SerializeField] GameEvent _event;
        [SerializeField] UnityEvent _action;

        void OnEnable()
        {
            _event.AddListener(this);
        }
        void OnDisable()
        {
            _event.RemoveListener(this);
        }
        public void OnEventTriggered()
        {
            _action.Invoke();
        }
    }
}