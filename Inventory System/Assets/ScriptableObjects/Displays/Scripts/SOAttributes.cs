﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InventorySystem.Items.Attributes
{
    [CreateAssetMenu(fileName = "NewAttributes", menuName = "Inventory System/Display/Attributes")]
    public class SOAttributes : ScriptableObject
    {
        List<Attribute> _attributes = new List<Attribute>();

        public void Reset()
        {
            _attributes = new List<Attribute>()
                {
                    new Attribute(AttributeName.Health, 100, 50, AttributeType.Spendable),
                    new Attribute(AttributeName.Mana, 100, 0, AttributeType.Spendable),
                    new Attribute(AttributeName.Strength, 100, 10, AttributeType.Default),
                    new Attribute(AttributeName.Defense, 100, 10, AttributeType.Default),
                    new Attribute(AttributeName.Perception, 100, 30, AttributeType.Default),
                    new Attribute(AttributeName.Endurance, 100, 20, AttributeType.Default),
                    new Attribute(AttributeName.Luck, 100, 50, AttributeType.Default)
                };
        }

        public int GetCount()
        {
            return _attributes.Count;
        }

        public Attribute GetAt(int index)
        {
            if (index < GetCount())
            {
                return _attributes[index];
            }
            else
            {
                return null;
            }
        }

        public void Add(AttributeName name, float value)
        {
            ApplyTo(name, value);
        }

        public void Remove(AttributeName name, float value)
        {
            ApplyTo(name, -value);
        }

        void ApplyTo(AttributeName name, float value)
        {
            foreach (var attribute in from Attribute attribute in _attributes
                                      where attribute.Name.Equals(name)
                                      select attribute)
            {
                attribute.Add(value);
            }
        }


        void ApplyBonuses(ItemObject item, int multiplier)
        {
            foreach (AttributeBonus bonus in item.Bonuses)
            {
                float value = bonus.Value;
                if (value != 0)
                {
                    foreach (var attribute in from Attribute attribute
                                              in _attributes
                                              where bonus.AttributeName.Equals(attribute.Name)
                                              select attribute)
                    {
                        if (multiplier == -1)
                        {
                            if (bonus.ApplyType.Equals(BonusApplyType.Default) ||
                                bonus.ApplyType.Equals(BonusApplyType.ApplyOverTime))
                            {
                                attribute.Remove(value);
                            }
                            else if (bonus.ApplyType.Equals(BonusApplyType.DefaultMax))
                            {
                                attribute.RemoveMax((int)value);
                            }

                            return;
                        }

                        switch (bonus.ApplyType)
                        {
                            case (BonusApplyType.Default):
                                attribute.Add(value);
                                break;
                            case (BonusApplyType.DefaultMax):
                                attribute.AddMax((int) value);
                                break;
                            case (BonusApplyType.ApplyOverTime):
                                attribute.ApplyBonusOverTime(bonus.Type, value, 5);
                                break;
                            case (BonusApplyType.Hold):
                                attribute.HoldBonusFor(bonus.Type, (int)value, 5);
                                break;
                            case (BonusApplyType.RampUpAndHold):
                                attribute.RampUpAndHoldFor(bonus.Type, value, 5, 5);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            DisplayUpdater.Instance.UpdateAttributesDisplay();
        }
        public void AddBonuses(ItemObject item)
        {
            ApplyBonuses(item, 1);
        }

        public void RemoveBonuses(ItemObject item)
        {
            ApplyBonuses(item, -1);
        }

    }
}
