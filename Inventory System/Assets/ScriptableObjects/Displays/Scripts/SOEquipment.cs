﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace InventorySystem.Items.Equipment
{
    [CreateAssetMenu(fileName = "NewEquipment", menuName = "Inventory System/Display/Equipment")]
    public class SOEquipment : ScriptableObject
    {
        [System.Serializable]
        private sealed class TypeAndObject
        {
            public EquipmentType Type;
            public EquipableObject Object;
            public float Condition;

            public TypeAndObject(EquipmentType t, EquipableObject o, float condition)
            {
                Type = t;
                Object = o;
                Condition = condition;
            }
        }

        [SerializeField] Dictionary<int, TypeAndObject> _equipedItems = new Dictionary<int, TypeAndObject>();

        readonly List<EquipmentType> _equipmentTypes = new List<EquipmentType>()
        {
            EquipmentType.Head,
            EquipmentType.Armor,
            EquipmentType.Weapon,
            EquipmentType.Shield,
            EquipmentType.Ring,
            EquipmentType.Ring,
            EquipmentType.Boots
        };

        public EquipmentType GetEquipmentTypeOnSlotNumber(int slotNumber)
        {
            return _equipmentTypes[slotNumber];
        }

        public void Clear()
        {
            _equipedItems.Clear();
        }

        public bool CanEquip(EquipmentType type)
        {
            int ringCount = 0;
            foreach (TypeAndObject ot in _equipedItems.Values)
                if (ot.Type.Equals(type))
                    if (type.Equals(EquipmentType.Ring) && ringCount == 0)
                        ringCount++;
                    else
                        return false;

            return true;
        }

        public bool HasEquiped(int slotNumber)
        {
            return _equipedItems.ContainsKey(slotNumber);
        }

        public (EquipableObject, float) GetItemAndConditionByType(EquipmentType type)
        {
            foreach (var ot in from TypeAndObject ot in _equipedItems.Values
                               where ot.Type.Equals(type)
                               select ot)
            {
                return (ot.Object, ot.Condition);
            }
            return (null, 0);
        }

        public float GetConditionBySlotNumber(int slotNumber)
        {
            return _equipedItems[slotNumber].Condition;
        }

        public EquipableObject GetItemBySlot(int slotNumber)
        {
            return _equipedItems[slotNumber].Object;
        }

        public void Equip(EquipableObject item, float condition)
        {
            EquipmentType type = item.EquipmentType;

            if (CanEquip(type))
            {
                for (int i = 0; i < _equipmentTypes.Count; i++)
                    if (_equipmentTypes[i].Equals(type))
                        if (!type.Equals(EquipmentType.Ring) || !HasEquiped(i))
                        {
                            _equipedItems[i] = new TypeAndObject(type, item, condition);
                            
                            GameManager.Instance.AnalyticsScript.OnItemEquippedAnalytics(
                                item.name, type.ToString());

                            break;
                        }
                DisplayUpdater.Instance.UpdateEquipmentDisplay();
            }
        }

        public void Unequip(EquipmentType type)
        {
            for (int i = 0; i < _equipmentTypes.Count; i++)
                if (_equipmentTypes[i].Equals(type))
                    _equipedItems.Remove(i);

            DisplayUpdater.Instance.UpdateEquipmentDisplay();
        }

        public void Unequip(int slotNumber)
        {
            _equipedItems.Remove(slotNumber);
            DisplayUpdater.Instance.UpdateEquipmentDisplay();
        }

        public void UpdateEquipmentCondition(float walkedDistance)
        {
            foreach (KeyValuePair<int, TypeAndObject> iot in _equipedItems)
            {
                iot.Value.Condition -= walkedDistance * iot.Value.Object.ItemWearOff;
                if (iot.Value.Condition <= 0)
                {
                    Unequip(iot.Key);
                    GameManager.Instance.RemoveBonuses(iot.Value.Object);
                }
                else if (ItemTooltip.Instance.isActiveAndEnabled && 
                         ItemTooltip.Instance.ItemSlotType.Equals(SlotType.Equipment) && 
                         ((EquipableObject) ItemTooltip.Instance.DisplayedItem).EquipmentType.Equals(
                             iot.Value.Object.EquipmentType)) {
                    ItemTooltip.Instance.UpdateText(iot.Value.Condition);
                }
            }

        }
    }
}
