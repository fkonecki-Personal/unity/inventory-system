﻿using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem.Items.Inventory
{
    [CreateAssetMenu(fileName = "NewInventory", menuName = "Inventory System/Display/Inventory")]
    public class SOInventory : ScriptableObject
    {
        List<InventorySlot> _inventoryItems = new List<InventorySlot>();

        public void Clear()
        {
            _inventoryItems.Clear();
        }

        public int GetCount()
        {
            return _inventoryItems.Count;
        }

        public InventorySlot GetAt(int index)
        {
            if (index < GetCount())
                return _inventoryItems[index];
            else
            {
                return null;
            }
        }

        public void AddItem(ItemObject item, int amount, float condition)
        {
            foreach (InventorySlot slot in _inventoryItems)
            {
                if (item.Type.Equals(ItemType.Equipable))
                {
                    break;
                }

                if (item.Type.Equals(ItemType.Unlimited) ||
                    (
                        item.Type.Equals(ItemType.Limited) &&
                        item.ItemName.Equals(slot.Item.ItemName) &&
                        slot.Amount < ((LimitedObject)item).GetLimit())
                    )
                {
                    slot.AddAmount(amount);
                    return;
                }
            }
            _inventoryItems.Add(new InventorySlot(item, amount, condition));
        }
        public void AddItemWithoutStacking(ItemObject item, int amount, float condition)
        {
            _inventoryItems.Add(new InventorySlot(item, amount, condition));
        }

        public void RemoveItemAt(int index)
        {
            RemoveItem(GetAt(index).Item);
        }

        public void RemoveAllItemsAt(int index)
        {
            InventorySlot inventorySlot = GetAt(index);
            ItemObject item = inventorySlot.Item;
            int amount = inventorySlot.Amount;

            for (int i = 0; i < amount; i++)
                RemoveItem(item);
        }

        public void DropItemAt(int index)
        {
            if (index < GetCount())
            {
                ItemObject item = GetAt(index).Item;
                GameObject prefab = item.Prefab;
                prefab.GetComponent<ItemScript>().Condition = GetAt(index).Condition;

                Instantiate(prefab,
                            RandomPositionInMinMaxPlayerRadius(0.8f, 1f),
                            Quaternion.identity,
                            GameObject.Find("Items").transform);

                RemoveItem(item);
            }

            DisplayUpdater.Instance.UpdateInventoryDisplay();
        }

        Vector3 RandomPositionInMinMaxPlayerRadius(float min, float max)
        {
            Vector3 playerPosition = GameObject.Find("Player").transform.position;
            playerPosition.z = 5;
            int angle = Random.Range(0, 360);

            return playerPosition +
                (
                    new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad),
                                Mathf.Sin(angle * Mathf.Deg2Rad),
                                0) *
                    Random.Range(min, max)
                );
        }

        void RemoveItem(ItemObject item)
        {
            for (int i = 0; i < _inventoryItems.Count; i++)
            {
                if (_inventoryItems[i].Item.ItemName.Equals(item.ItemName))
                {
                    if (_inventoryItems[i].Amount > 1)
                        _inventoryItems[i].RemoveAmount(1);
                    else
                        _inventoryItems.RemoveAt(i);

                    return;
                }
            }
        }

        public void Sort()
        {
            _inventoryItems.Sort(SortCompare);
        }
        int SortCompare(InventorySlot x, InventorySlot y)
        {
            int result = ((int)(x.Item.Type)).CompareTo((int)(y.Item.Type));
            return result != 0 ? result : (x.Item.ItemName).CompareTo(y.Item.ItemName);
        }
        public void SwitchItemsInInventory(int inAirSlotNumber,
                                           int intoSlotNumber)
        {
            if (intoSlotNumber < GetCount())
            {
                (_inventoryItems[intoSlotNumber], _inventoryItems[inAirSlotNumber]) =
                (_inventoryItems[inAirSlotNumber], _inventoryItems[intoSlotNumber]);
            }

            ItemTooltip.Instance.StopDragging();
            DisplayUpdater.Instance.UpdateInventoryDisplay();
        }

        public EquipmentType GetEquipmentTypeForItemAt(int inAirSlotNumber)
        {
            return ((EquipableObject)_inventoryItems[inAirSlotNumber].Item).EquipmentType;
        }
    }
}
