﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "LimitedObject", menuName = "Inventory System/Items/Limited")]
    public class LimitedObject : StackableObject
    {
        [SerializeField] int _limit;

        void Awake()
        {
            _itemName = _prefab.name;
            _type = ItemType.Limited;
        }

        public int GetLimit() { return _limit; }
    }
}