﻿namespace InventorySystem.Items
{
    [System.Serializable]
    public enum ItemType
    {
        Permanent,
        Pickupable,
        Equipable,
        NonEquipable,
        Stackable,
        Limited,
        Unlimited
    }
}