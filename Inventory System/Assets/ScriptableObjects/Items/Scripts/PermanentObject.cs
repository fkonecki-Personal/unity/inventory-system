﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "PermanentObject", menuName = "Inventory System/Items/Permanent")]
    public class PermanentObject : ItemObject
    {
        void Awake()
        {
            _itemName = _prefab.name;
            _type = ItemType.Permanent;
        }
    }
}
