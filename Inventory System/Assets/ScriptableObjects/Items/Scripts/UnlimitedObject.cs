﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "UnlimitedObject", menuName = "Inventory System/Items/Unlimited")]
    public class UnlimitedObject : StackableObject
    {
        void Awake()
        {
            _itemName = _prefab.name;
            _type = ItemType.Unlimited;
        }
    }
}
