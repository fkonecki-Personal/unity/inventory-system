﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "NonEquipableObject", menuName = "Inventory System/Items/NonEquipable")]
    public abstract class NonEquipableObject : PickupableObject
    {
    }
}