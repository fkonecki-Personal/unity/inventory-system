﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "Pickupable Object", menuName = "Inventory System/Items/Pickupable")]
    public abstract class PickupableObject : ItemObject
    {
    }
}