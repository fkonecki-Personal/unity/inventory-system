﻿using System.Collections.Generic;
using UnityEngine;

using InventorySystem.Items.Attributes;

namespace InventorySystem.Items
{
    [System.Serializable]
    public abstract class ItemObject : ScriptableObject
    {
        [SerializeField] protected string _itemName;
        [SerializeField] protected ItemType _type;
        [SerializeField] protected GameObject _prefab;

        [SerializeField] protected List<AttributeBonus> _bonuses = 
            new List<AttributeBonus>()
                {
                    new AttributeBonus(AttributeName.Health),
                    new AttributeBonus(AttributeName.Mana),
                    new AttributeBonus(AttributeName.Strength),
                    new AttributeBonus(AttributeName.Defense),
                    new AttributeBonus(AttributeName.Perception),
                    new AttributeBonus(AttributeName.Endurance),
                    new AttributeBonus(AttributeName.Luck)
                };

        public string ItemName
        {
            get => _itemName;
            set => _itemName = value;
        }
        public ItemType Type { get => _type; }
        public GameObject Prefab { get => _prefab; }

        public List<AttributeBonus> Bonuses { get => _bonuses; }
    }
}