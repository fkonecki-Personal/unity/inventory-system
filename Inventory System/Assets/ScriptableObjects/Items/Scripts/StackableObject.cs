﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "StackableObject", menuName = "Inventory System/Items/Stackable")]
    public abstract class StackableObject : NonEquipableObject
    {
    }
}
