﻿namespace InventorySystem.Items
{
	[System.Serializable]
	public enum EquipmentType
	{
		Head,
		Armor,
		Weapon,
		Shield,
		Ring,
		Boots
	}
}