﻿using UnityEngine;

namespace InventorySystem.Items
{
    [CreateAssetMenu(fileName = "EquipableObject", menuName = "Inventory System/Items/Equipable")]
    public class EquipableObject : PickupableObject
    {
        [SerializeField] EquipmentType _equipmentType;
        [SerializeField] float _itemWearOff = 1.0f;

        public EquipmentType EquipmentType
        {
            get => _equipmentType;
            set => _equipmentType = value;
        }
        public float ItemWearOff {
            get => _itemWearOff; 
            set => _itemWearOff = value;
        }

        void Awake()
        {
            _itemName = _prefab.name;
            _type = ItemType.Equipable;
        }
    }
}