﻿using System.Collections.Generic;
using UnityEngine;

#if ENABLE_CLOUD_SERVICES_ANALYTICS
using UnityEngine.Analytics;
#endif

namespace InventorySystem
{
    public class AnalyticsScript : MonoBehaviour
    {
        float _lastCollisionTime = 0;
        int _playerMovementAnalyticsSentCounter = 0;

        void Start()
        {
            _lastCollisionTime = 0;
            _playerMovementAnalyticsSentCounter = 0;
        }

        public void OnStartupAnalytics()
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            Analytics.CustomEvent("Startup", new Dictionary<string, object>
            {
                { "platform", Application.platform.ToString() },
                { "localTime", Time.time }
            });

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }

        public void OnItemPickedUpAnalytics(string itemName, string itemType)
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            Analytics.CustomEvent("Item picked up", new Dictionary<string, object>
            {
                { "itemName", itemName },
                { "itemType", itemType }
            });

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }

        public void OnItemEquippedAnalytics(string itemName, string itemSlot)
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            Analytics.CustomEvent("Item equipped", new Dictionary<string, object>
            {
                { "itemName", itemName },
                { "itemSlot", itemSlot }
            });

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }

        public void OnItemUsedAnalytics(string itemName, string itemType)
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            Analytics.CustomEvent("Item used", new Dictionary<string, object>
            {
                { "itemName", itemName },
                { "itemType", itemType }
            });

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }

        public void OnWindowOpenedAnalytics(string windowType)
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            Analytics.CustomEvent("Window opened", new Dictionary<string, object>
            {
                { "windowType", windowType }
            });

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }

        public void OnPlayerMoved10UnitsAnalytics()
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            if (_playerMovementAnalyticsSentCounter++ < 5)
                Analytics.CustomEvent("Player moved 10 units");

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }

        public void OnPlayerCollidedAnalytics()
        {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
            if (Time.time - _lastCollisionTime > 5)
            {
                _lastCollisionTime = Time.time;
                Analytics.CustomEvent("Player collided");
            }

            if (Application.platform.Equals(RuntimePlatform.WebGLPlayer))
                Analytics.FlushEvents();
#endif
        }
    }
}