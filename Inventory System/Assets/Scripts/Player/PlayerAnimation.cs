﻿using UnityEngine;

namespace InventorySystem.Player
{
    public class PlayerAnimation : MonoBehaviour
    {
        Animator _animator;

        void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void SetAnimatorWalkingParams(float horizontal, float vertical)
        {
            vertical = horizontal != 0 ? 0 : vertical;

            bool isWalking = horizontal != 0 || vertical != 0;

            _animator.SetBool("Walking", isWalking);

            _animator.SetFloat("Horizontal", horizontal);
            _animator.SetFloat("Vertical", vertical);
        }
    }
}