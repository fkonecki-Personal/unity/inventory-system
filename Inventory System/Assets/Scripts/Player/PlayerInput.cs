﻿using UnityEngine;

namespace InventorySystem.Player
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] Joystick joystick;

        public (float, float) GetHorizontalAndVerical()
        {
            if (GameManager.Instance.IsTouchEnabled)
                return (joystick.Horizontal, joystick.Vertical);
            else
                return (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
    }

}