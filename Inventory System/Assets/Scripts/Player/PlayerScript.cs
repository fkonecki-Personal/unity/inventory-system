﻿using UnityEngine;

namespace InventorySystem.Player
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class PlayerScript : MonoBehaviour
	{
		bool _isInputEnabled = true;

		float _horizontalInput;
		float _verticalInput;

		Vector2 _currentPosition;
		Vector2 _lastPosition;

		float _distanceSinceLast10Units = 0;

		[SerializeField] PlayerInput _playerInput;
		[SerializeField] PlayerMovement _playerMovement;
		[SerializeField] PlayerAnimation _playerAnimation;
		[SerializeField] PlayerCollision _playerCollision;

		void Start()
		{
			_lastPosition = transform.position;
			_currentPosition = transform.position;
		}

		void Update()
		{
			if (_isInputEnabled)
			{
				(float h, float v) = _playerInput.GetHorizontalAndVerical();

				if (h != _horizontalInput || v != _verticalInput)
				{
					_horizontalInput = h;
					_verticalInput = v;

					_playerAnimation.SetAnimatorWalkingParams(_horizontalInput, _verticalInput);
					_playerCollision.SetHasInput(_horizontalInput != 0 || _verticalInput != 0);
				}
			}
		}

		void FixedUpdate()
		{
			_playerMovement.Move(_horizontalInput, _verticalInput);
			_playerMovement.UpdateEquipmentCondition(WalkedDistance());
			_playerCollision.HandleCollision();

			_distanceSinceLast10Units += WalkedDistance();
			if (_distanceSinceLast10Units > 10) {
				_distanceSinceLast10Units = 0;
				GameManager.Instance.AnalyticsScript.OnPlayerMoved10UnitsAnalytics();
			}
		}

		public void EnableInput()
		{
			_isInputEnabled = true;
		}

		public void DisableInput()
		{
			_isInputEnabled = false;

			_horizontalInput = 0;
			_verticalInput = 0;

			_playerMovement.Move(0, 0);
			_playerCollision.SetHasInput(false);
			_playerAnimation.SetAnimatorWalkingParams(0, 0);
		}

		float WalkedDistance()
		{
			_lastPosition = _currentPosition;
			_currentPosition = transform.position;

			return (_currentPosition - _lastPosition).magnitude;
		}
	}
}