﻿using UnityEngine;

using InventorySystem.Items;
using InventorySystem.Items.Attributes;
using InventorySystem.Items.Equipment;
using InventorySystem.Items.Inventory;

namespace InventorySystem.Player
{
	public class PlayerCollision : MonoBehaviour
	{
		CollisionCheckType _collisionCheckType = CollisionCheckType.TRIGGER_C;

		[SerializeField] SOInventory _inventory;
		[SerializeField] SOEquipment _equipment;
		[SerializeField] SOAttributes _attributes;

		[SerializeField] GameEvent _itemPickedUp;

		bool _hasInput;

		void OnColliderEnter2D(Collider2D collider) {
			if (!collider.isTrigger)
				GameManager.Instance.AnalyticsScript.OnPlayerCollidedAnalytics();
		}

		void OnTriggerEnter2D(Collider2D collision)
		{
			if (_collisionCheckType.Equals(CollisionCheckType.TRIGGER_C) ||
				(_collisionCheckType.Equals(CollisionCheckType.TRIGGER_I) && _hasInput))
			{
				GameObject other = collision.gameObject;

				if (other.CompareTag("Item"))
				{
					HandleCollision(collision);
					Destroy(other);
				}
			}
		}

		public void SetHasInput(bool hasInput)
		{
			_hasInput = hasInput;
		}


		public void SetCollisionCheckType(int collisionCheckType)
		{
			_collisionCheckType = (CollisionCheckType)collisionCheckType;
		}

		public void HandleCollision()
		{
			if (_collisionCheckType.Equals(CollisionCheckType.OVERLAP_C) ||
				(_collisionCheckType.Equals(CollisionCheckType.OVERLAP_I) && _hasInput))
			{
				foreach (Collider2D collider in
					Physics2D.OverlapCircleAll(transform.position, .5f, LayerMask.GetMask("Item")))
				{
					GameObject other = collider.gameObject;
					HandleCollision(collider);
					Destroy(other);
				}
			}
		}

		public void HandleCollision(Collider2D collision)
		{
			GameObject other = collision.gameObject;
			ItemScript itemScript = other.GetComponent<ItemScript>();

			if (itemScript && other.CompareTag("Item"))
			{
				ItemObject item = itemScript.Item;
				float condition = itemScript.Condition;

				GameManager.Instance.AnalyticsScript.OnItemPickedUpAnalytics(
					item.ItemName, item.Type.ToString());

				if (item.Type.Equals(ItemType.Permanent))
				{
					Debug.Log("Picked up permanent object: " + item.ItemName);
					_attributes.AddBonuses(item);
				}
				else
				{
					if (item.Type.Equals(ItemType.Equipable))
					{
						EquipableObject equipableItem = (EquipableObject)item;
						if (_equipment.CanEquip(equipableItem.EquipmentType))
						{
							_equipment.Equip(equipableItem, condition);
							_attributes.AddBonuses(item);
						}
						else
						{
							_inventory.AddItem(item, 1, itemScript.Condition);
						}
					}
					else
					{
						_inventory.AddItem(item, 1, itemScript.Condition);
					}
				}
				_itemPickedUp.TriggerEvent();
			}
		}
	}
}