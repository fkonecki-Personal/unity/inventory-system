﻿namespace InventorySystem.Player
{ 
	[System.Serializable]
	public enum CollisionCheckType
	{
		TRIGGER_C,
		OVERLAP_C,
		TRIGGER_I,
		OVERLAP_I
	}
}
