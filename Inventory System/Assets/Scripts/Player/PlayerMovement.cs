﻿using UnityEngine;

using InventorySystem.Items.Equipment;

namespace InventorySystem.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        const float MOVEMENT_SPEED = 1.4f;
        Rigidbody2D _rb2D;

        [SerializeField] SOEquipment _equipment;

        void Start()
        {
            _rb2D = GetComponent<Rigidbody2D>();
        }

        public void Move(float h, float v)
        {
            Vector2 moveDirection = new Vector2(h, h != 0 ? 0 : v);
            _rb2D.velocity = moveDirection * MOVEMENT_SPEED;
        }

        public void UpdateEquipmentCondition(float walkedDistance)
        {
            _equipment.UpdateEquipmentCondition(walkedDistance);
        }
    }
}