﻿using UnityEngine;

namespace InventorySystem
{
    public class DisplayUpdater : MonoBehaviour
    {
        public static DisplayUpdater Instance;

        [SerializeField] GameObject _attributesDisplay;
        [SerializeField] GameObject _equipmentDisplay;
        [SerializeField] GameObject _inventoryDisplay;


        [SerializeField] GameEvent _updateInventoryDisplay;
        [SerializeField] GameEvent _updateEquipmentDisplay;
        [SerializeField] GameEvent _updateAttributesDisplay;

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void UpdateInventoryDisplay()
        {
            if (_inventoryDisplay.activeSelf)
            {
                _updateInventoryDisplay.TriggerEvent();
            }
        }
        public void UpdateEquipmentDisplay()
        {
            if (_equipmentDisplay.activeSelf)
            {
                _updateEquipmentDisplay.TriggerEvent();
            }
        }

        public void UpdateAttributesDisplay()
        {
            if (_attributesDisplay.activeSelf)
            {
                _updateAttributesDisplay.TriggerEvent();
            }
        }

        public void UpdateAllDisplays()
        {
            UpdateInventoryDisplay();
            UpdateEquipmentDisplay();
            UpdateAttributesDisplay();
        }
    }
}
