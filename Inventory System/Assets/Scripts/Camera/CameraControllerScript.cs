﻿using UnityEngine;

namespace InventorySystem.Camera
{
    public class CameraControllerScript : MonoBehaviour
    {
        Transform _player;
        Transform _followTarget;

        Vector3 _playerOffset;

        const float PLAYER_SMOOTH_SPEED = 0.09f;
        const float CINEMATIC_SMOOTH_SPEED = 0.03f;

        [SerializeField] Transform _cinematicTarget;
        [SerializeField] GameEvent _cinematicEnded;

        float _smoothSpeed;

        void Start()
        {
            _player = GameObject.Find("Player").transform;
            _followTarget = _player;
            _smoothSpeed = PLAYER_SMOOTH_SPEED;

            _playerOffset = transform.position - _player.transform.position;
        }

        void FixedUpdate()
        {
            Vector3 targetPosition = _followTarget.position + _playerOffset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, targetPosition, _smoothSpeed);
            transform.position = smoothedPosition;
        }

        //******************************************

        public void StartCinematicAnimation()
        {
            GetComponent<Animator>().SetBool("CinematicRunning", true);
        }

        public void CinematicStarted()
        {
            _followTarget = _cinematicTarget;
            _smoothSpeed = CINEMATIC_SMOOTH_SPEED;
        }

        public void SetPlayerAsTarget()
        {
            _followTarget = _player;
            GetComponent<Animator>().SetBool("CinematicRunning", false);
        }

        public void CinematicEnded()
        {
            _smoothSpeed = PLAYER_SMOOTH_SPEED;
            _cinematicEnded.TriggerEvent();
        }
    }
}
