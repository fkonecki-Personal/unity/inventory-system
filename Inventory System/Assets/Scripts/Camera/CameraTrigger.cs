﻿using UnityEngine;

namespace InventorySystem.Camera
{
	public class CameraTrigger : MonoBehaviour
	{
		[SerializeField] GameEvent cinematicStarted;

		bool _isPlayerOutside = true;

		void OnTriggerEnter2D(Collider2D collision)
		{
			GameObject other = collision.gameObject;

			if (other.CompareTag("Player") && _isPlayerOutside)
			{
				_isPlayerOutside = false;
				GetComponent<CircleCollider2D>().radius += 0.1f;

				cinematicStarted.TriggerEvent();
			}
		}

		void OnTriggerExit2D(Collider2D collision)
		{
			GameObject other = collision.gameObject;

			if (other.CompareTag("Player"))
			{
				GetComponent<CircleCollider2D>().radius -= 0.1f;
				_isPlayerOutside = true;
			}
		}


	}
}