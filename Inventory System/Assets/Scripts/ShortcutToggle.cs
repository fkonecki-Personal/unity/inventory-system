﻿using UnityEngine;

namespace InventorySystem
{
    public class ShortcutToggle : MonoBehaviour
    {
        [SerializeField] KeyCode _shortcut;
        [SerializeField] GameObject _target1;
        [SerializeField] GameObject _target2;

        void Update()
        {
            if (Input.GetKeyDown(_shortcut) && GameManager.Instance.IsInputEnabled)
            {
                _target1.SetActive(!_target1.activeSelf);
                _target2.SetActive(!_target2.activeSelf);
            }
        }

    }
}