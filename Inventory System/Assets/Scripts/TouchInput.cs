﻿using UnityEngine;

using InventorySystem.Items;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace InventorySystem
{
    public class TouchInput : MonoBehaviour
    {
        [SerializeField] GameEvent _doubleTap;
        [SerializeField] GameEvent _longPress;
        [SerializeField] GameEvent _release;

        bool _isTouching = false;
        bool _isLongTouch = false;
        float _touchDistance = 0;
        float _touchStart = 0;

        public bool IsTouching
        {
            get => _isTouching;
            set => _isTouching = value;
        }

        public bool IsLongTouch
        {
            get => _isLongTouch;
            set => _isLongTouch = value;
        }

        void Start()
        {
            IsTouching = false;
            IsLongTouch = false;
        }

        void Update()
        {
            IsTouching = Input.touchCount > 0;

            if (!IsTouching)
            { IsLongTouch = false; }

            if (IsTouching)
            {
                if (Input.touchCount == 2)
                {
                    if (Input.touches[1].phase.Equals(TouchPhase.Began))
                    {
                        _touchDistance = (Input.touches[0].position - Input.touches[1].position).sqrMagnitude;
                    }
                    float distance = 
                        (Input.touches[0].position - Input.touches[1].position).sqrMagnitude;

                    UnityEngine.Camera.main.orthographicSize =
                        Mathf.Lerp(3, 1, (distance / _touchDistance) - 0.5f);

                    return;
                }

                float now = Time.time;
                if (Input.touches[0].phase.Equals(TouchPhase.Began))
                {
                    if (now - _touchStart < 0.5f)
                    {
                        _doubleTap.TriggerEvent();
                    }

                    _touchStart = now;
                }
                else if (Input.touches[0].phase.Equals(TouchPhase.Ended))
                {

                    if (IsReleaseOverUIObject(Input.touches[0].position))
                    {
                        ItemTooltip.Instance._dropZone
                                   .GetComponent<DropZone>().OnTouchReleaseClick();
                        ItemTooltip.Instance._dropZone.SetActive(false);
                    }
                    _release.TriggerEvent();

                }
                else if (now - _touchStart > 1f && !IsLongTouch)
                {
                    IsLongTouch = true;
                    ItemTooltip.Instance._dropZone.SetActive(true);
                    _longPress.TriggerEvent();
                }
            }
        }

        public bool IsReleaseOverUIObject(Vector2 position)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = position;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count == 1;
        }
    }
}