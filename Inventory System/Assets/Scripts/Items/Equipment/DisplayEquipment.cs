﻿using UnityEngine;

namespace InventorySystem.Items.Equipment
{
    public class DisplayEquipment : MonoBehaviour
    {
        [SerializeField] SOEquipment _equipment;
        [SerializeField] GameObject _screen;

        bool _hasStarted = false;

        void Start()
        {
            _equipment.Clear();
            _screen.SetActive(false);
            _hasStarted = true;
        }

        void OnEnable()
        {
            UpdateDisplay();
            if (_hasStarted)
                GameManager.Instance.AnalyticsScript.OnWindowOpenedAnalytics("EquipmentWindow");
        }

        public void UpdateDisplay()
        {
            for (int i = 0; i < 7; i++)
            {
                GameObject equipmentObject = gameObject.transform.GetChild(i).gameObject;
                UnityEngine.UI.Image objectImage;

                try
                {
                    objectImage = equipmentObject.transform.GetChild(1).gameObject.GetComponent<UnityEngine.UI.Image>();
                } catch (System.Exception e) {
                    continue;
                }

                if (_equipment.HasEquiped(i))
                {
                    equipmentObject.GetComponentInChildren<UnityEngine.UI.Text>().text = 
                        _equipment.GetItemBySlot(i).ItemName;
                    Sprite sprite = _equipment.GetItemBySlot(i).Prefab.GetComponent<SpriteRenderer>().sprite;

                    objectImage.sprite = sprite;
                    objectImage.color = Color.white;
                }
                else
                {
                    objectImage.sprite = null;
                    objectImage.color = Color.clear;
                }
            }
        }
    }
}
