﻿using UnityEngine;
using UnityEngine.EventSystems;

using InventorySystem.Items.Inventory;
using InventorySystem.Items.Equipment;
using InventorySystem.Items.Attributes;

namespace InventorySystem.Items
{
	public class ItemSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
	{
		Canvas _canvas;

		[SerializeField] SOInventory _inventory;
		[SerializeField] SOEquipment _equipment;
		[SerializeField] SOAttributes _attributes;

		public SlotType Type;
		public int SlotNumber;

		Color _highlightColor = Color.green - new Color(0, 0, 0, 0.5f);

		Color _initialColor;
		bool _isBeingViewed = false;

		public bool IsBeingViewed
		{
			get => _isBeingViewed;
			set => _isBeingViewed = value;
		}

		void Start()
		{
			_canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
			_initialColor = transform.GetComponentInParent<UnityEngine.UI.Image>().color;
		}

		void Update()
		{
			if (_isBeingViewed && !IsSlotEmpty() && !ItemTooltip.Instance.IsItemBeingDragged)
			{
				if (Input.GetKeyDown(KeyCode.Alpha4))
					Use();
				else if (Input.GetKeyDown(KeyCode.Alpha5))
					Equip();
				else if (Input.GetKeyDown(KeyCode.Alpha6))
					Unequip();
				else if (Input.GetKeyDown(KeyCode.Alpha7))
					AddToSplitter();
				else if (Input.GetKeyDown(KeyCode.Alpha8))
					Drop();
			}
		}

		public void Use()
		{
			if (_isBeingViewed && !IsSlotEmpty() && Type.Equals(SlotType.Inventory))
			{
				ItemObject item = _inventory.GetAt(SlotNumber).Item;
				if (item as LimitedObject || item as UnlimitedObject)
				{
					_isBeingViewed = false;
					ItemTooltip.Instance.HideTooltip();

					_attributes.AddBonuses(item);
					_inventory.RemoveItemAt(SlotNumber);

					GameManager.Instance.AnalyticsScript.OnItemUsedAnalytics(
						item.ItemName, item.Type.ToString());

					DisplayUpdater.Instance.UpdateInventoryDisplay();
					DisplayUpdater.Instance.UpdateAttributesDisplay();
				}
			}
		}

		public void Equip()
		{
			if (_isBeingViewed && !IsSlotEmpty() && Type.Equals(SlotType.Inventory))
			{
				InventorySlot inventorySlot = _inventory.GetAt(SlotNumber);
				ItemObject item = inventorySlot.Item;
				float condition = inventorySlot.Condition;

				if (item as EquipableObject)
				{
					EquipableObject equipableItem = (EquipableObject)item;

					_isBeingViewed = false;
					ItemTooltip.Instance.HideTooltip();

					if (!_equipment.CanEquip(equipableItem.EquipmentType))
					{
						EquipmentType equipmentType = equipableItem.EquipmentType;
						(EquipableObject equipedItem, float equipedCondition) =
							_equipment.GetItemAndConditionByType(equipmentType);

						_inventory.AddItem(equipedItem, 1, equipedCondition);
						_attributes.RemoveBonuses(equipedItem);
						_equipment.Unequip(equipmentType);
					}

					_equipment.Equip(equipableItem, condition);
					_inventory.RemoveItemAt(SlotNumber);
					_attributes.AddBonuses(item);

					UpdateAllScreens();
				}
			}
		}
		public void Unequip()
		{
			if (_isBeingViewed && !IsSlotEmpty() && Type.Equals(SlotType.Equipment))
			{
				_isBeingViewed = false;
				ItemTooltip.Instance.HideTooltip();

				float condition = _equipment.GetConditionBySlotNumber(SlotNumber);
				EquipableObject equipedItem = _equipment.GetItemBySlot(SlotNumber);

				_inventory.AddItem(equipedItem, 1, condition);
				_attributes.RemoveBonuses(equipedItem);
				_equipment.Unequip(SlotNumber);

				UpdateAllScreens();
			}
		}

		public void AddToSplitter()
		{
			if (Type.Equals(SlotType.Inventory))
			{
				ItemTooltip.Instance._splitter.SetActive(true);
				ItemTooltip.Instance.SplitterSlot
					.GetComponent<SplitterSlot>().AddFromInventoryToSplitter(SlotNumber);
			}
		}

		public void Drop()
		{
			if (_isBeingViewed && !IsSlotEmpty() && Type.Equals(SlotType.Inventory))
			{
				_isBeingViewed = false;
				ItemTooltip.Instance.HideTooltip();

				_inventory.RemoveItemAt(SlotNumber);
				UpdateAllScreens();
			}
		}

		void EquipItemFromInventory(int inAirSlotNumber, int intoSlotNumber)
		{
			if (_equipment.HasEquiped(intoSlotNumber))
			{
				UnequipItem(intoSlotNumber, _equipment.GetItemBySlot(intoSlotNumber));
			}

			InventorySlot inventorySlot = _inventory.GetAt(inAirSlotNumber);
			ItemObject item = inventorySlot.Item;
			float condition = inventorySlot.Condition;

			_equipment.Equip((EquipableObject)item, condition);
			_attributes.AddBonuses(_inventory.GetAt(inAirSlotNumber).Item);
			_inventory.RemoveItemAt(inAirSlotNumber);

			UpdateAllScreens();
		}

		void UnequipItem(int inAirSlotNumber, ItemObject item)
		{
			float condition = _equipment.GetConditionBySlotNumber(inAirSlotNumber);
			_equipment.Unequip(inAirSlotNumber);
			_attributes.RemoveBonuses(item);
			_inventory.AddItem(item, 1, condition);

			UpdateAllScreens();
		}

		void UpdateAllScreens()
		{
			DisplayUpdater.Instance.UpdateAllDisplays();
		}

		bool DoesDraggedEquipmentTypeMatchSlotType()
		{
			return _inventory.GetEquipmentTypeForItemAt(ItemTooltip.Instance.ItemSlotNumber)
				.Equals(_equipment.GetEquipmentTypeOnSlotNumber(SlotNumber));
		}

		public bool IsSlotEmpty()
		{
			return Type.Equals(SlotType.Inventory) ?
					SlotNumber >= _inventory.GetCount() :
					!_equipment.HasEquiped(SlotNumber);
		}

		(ItemObject, float) GetItemAndConditionOnThisSlot()
		{
			if (Type.Equals(SlotType.Inventory))
				return (_inventory.GetAt(SlotNumber).Item, _inventory.GetAt(SlotNumber).Condition);
			else
				return (_equipment.GetItemBySlot(SlotNumber), _equipment.GetConditionBySlotNumber(SlotNumber));
		}


// Touch input ************************************************

		public void OnDoubleTap()
		{
			if (_isBeingViewed && !IsSlotEmpty() && !ItemTooltip.Instance.IsItemBeingDragged)
			{
				if (Type.Equals(SlotType.Inventory))
				{
					if (_inventory.GetAt(SlotNumber).Item.Type.Equals(ItemType.Equipable))
						Equip();
					else 
						Use();
				}
				else Unequip();
			}
			_isBeingViewed = false;

		}

		public void OnRelease()
		{
			if (GameManager.Instance.LastViewedInventorySlot == SlotNumber &&
				ItemTooltip.Instance.IsItemBeingDragged)

				if (ItemTooltip.Instance.ItemSlotType.Equals(SlotType.Inventory))
				{
					if (Type.Equals(SlotType.Inventory) &&
						!ItemTooltip.Instance.ItemSlotNumber.Equals(SlotNumber))
					{
						_inventory.SwitchItemsInInventory(ItemTooltip.Instance.ItemSlotNumber, SlotNumber);
					}
				}
		}

// Pointer actions *************************************************

		public void OnPointerClick(PointerEventData eventData)
		{
			if (!GameManager.Instance.IsTouchEnabled && 
				!ItemTooltip.Instance.IsItemBeingDragged)
			{
				if (eventData.button == PointerEventData.InputButton.Middle)
					Use();
				else if (eventData.button == PointerEventData.InputButton.Right)
				{
					if (Type.Equals(SlotType.Inventory))
						Equip();
					else
						Unequip();
				}
			}
			else if (eventData.button == PointerEventData.InputButton.Left)
			{
				if (ItemTooltip.Instance.ItemSlotType.Equals(SlotType.Inventory))
				{
					if (Type.Equals(SlotType.Inventory) && !ItemTooltip.Instance.ItemSlotNumber.Equals(SlotNumber))
					{
						_inventory.SwitchItemsInInventory(ItemTooltip.Instance.ItemSlotNumber, SlotNumber);
					}
					else if (Type.Equals(SlotType.Equipment) && DoesDraggedEquipmentTypeMatchSlotType())
					{
						EquipItemFromInventory(ItemTooltip.Instance.ItemSlotNumber, SlotNumber);
					}
				} 
				else if (Type.Equals(SlotType.Inventory))
				{
					int inAirSlotNumber = ItemTooltip.Instance.ItemSlotNumber;
					ItemObject item = _equipment.GetItemBySlot(inAirSlotNumber);

					ItemTooltip.Instance.DraggedImage.GetComponent<Drag>().ReturnItem();
					UnequipItem(inAirSlotNumber, item);
				}

				ItemTooltip.Instance.StopDragging();
				UpdateAllScreens();
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			_isBeingViewed = true;
			transform.GetComponentInParent<UnityEngine.UI.Image>().color = _highlightColor;

			if (!IsSlotEmpty() && !ItemTooltip.Instance.IsItemBeingDragged)
			{
				(ItemObject item, float condition) = GetItemAndConditionOnThisSlot();
				ItemTooltip.Instance.ShowTooltip(item, condition, Type);
			}
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (GameManager.Instance.IsTouchEnabled)
				GameManager.Instance.LastViewedInventorySlot = SlotNumber;

			_isBeingViewed = false;
			transform.GetComponentInParent<UnityEngine.UI.Image>().color = _initialColor;
			ItemTooltip.Instance.HideTooltip();
		}
	}

}