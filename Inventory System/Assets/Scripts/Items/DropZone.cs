﻿using UnityEngine;
using UnityEngine.EventSystems;

using InventorySystem.Items.Equipment;
using InventorySystem.Items.Inventory;

namespace InventorySystem.Items
{
    public class DropZone : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] SOInventory _inventory;
        [SerializeField] SOEquipment _equipment;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!GameManager.Instance.IsTouchEnabled && 
                ItemTooltip.Instance.IsItemBeingDragged)
            {
                if (ItemTooltip.Instance.ItemSlotType.Equals(SlotType.Equipment))
                {
                    _equipment.Unequip(ItemTooltip.Instance.ItemSlotNumber);
                }
                else
                {
                    _inventory.DropItemAt(ItemTooltip.Instance.ItemSlotNumber);
                }
                ItemTooltip.Instance.StopDragging();
            }
        }

        public void OnTouchReleaseClick()
        {
            if (ItemTooltip.Instance.IsItemBeingDragged)
            {
                if (ItemTooltip.Instance.ItemSlotType.Equals(SlotType.Equipment))
                {
                    _equipment.Unequip(ItemTooltip.Instance.ItemSlotNumber);
                }
                else
                {
                    _inventory.DropItemAt(ItemTooltip.Instance.ItemSlotNumber);
                }
                ItemTooltip.Instance.StopDragging();
            }
        }
    }
}