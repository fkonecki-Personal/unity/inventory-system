﻿namespace InventorySystem.Items.Attributes
{
    [System.Serializable]
    public enum AttributeType
    { 
        Default,
        Spendable
    }
}
