﻿using UnityEngine;

namespace InventorySystem.Items.Attributes
{
    public class DisplayAttributes : MonoBehaviour
    {
        [SerializeField] SOAttributes _attributes;

        [SerializeField] GameObject _screen;

        readonly Color INITIAL_COLOR = Color.yellow;
        readonly Color FULL_COLOR = Color.green;

        bool _hasStarted = false;

        void Start()
        {
            _attributes.Reset();
            _screen.SetActive(false);
            _hasStarted = true;
        }

        void OnEnable()
        {
            UpdateDisplay();
            if (_hasStarted)
                GameManager.Instance.AnalyticsScript.OnWindowOpenedAnalytics("AttributeWindow");
        }

        public void UpdateDisplay()
        {
            for (int i = 0; i < _attributes.GetCount(); i++)
            {
                Attribute attribute = _attributes.GetAt(i);

                string name = attribute.Name.ToString();
                int value = (int) Mathf.Min(attribute.CurrentValue, 100);

                UnityEngine.UI.Text attributeTextObject =
                    gameObject.transform.Find(name).gameObject.GetComponent<UnityEngine.UI.Text>();

                attributeTextObject.text = name + " : " + value;

                attributeTextObject.color = value < 100 ? INITIAL_COLOR : FULL_COLOR;
            }
        }

    }
}
