﻿namespace InventorySystem.Items.Attributes
{
    [System.Serializable]
    public enum AttributeName
    {
        Health,
        Mana,
        Strength,
        Defense,
        Perception,
        Endurance,
        Luck
    }
}
