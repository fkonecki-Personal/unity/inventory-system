﻿using UnityEngine;
using System.Collections.Generic;

namespace InventorySystem.Items.Attributes
{
    [System.Serializable]
    public class Attribute
    {
        private static readonly Dictionary<AttributeName, string> ATTRIBUTE_NAMES =
            new Dictionary<AttributeName, string>()
            {
                { AttributeName.Health, "Health" },
                { AttributeName.Mana, "Mana" },
                { AttributeName.Strength, "Strength" },
                { AttributeName.Defense, "Defense" },
                { AttributeName.Perception, "Perception" },
                { AttributeName.Endurance, "Endurance" },
                { AttributeName.Luck, "Luck" },
            };

        AttributeType _type;
        AttributeName _name;
        int _maxValue;
        float _currentValue;

        public AttributeName Name { get => _name; }
        public float CurrentValue
        {
            get => _currentValue;
            set => _currentValue = value;
        }

        public Attribute(AttributeName name, int maxValue, int currentValue, AttributeType type)
        {
            _name = name;
            _maxValue = maxValue;
            _currentValue = currentValue;
            _type = type;
        }

        public void AddMax(int value)
        {
            _maxValue += value;
        }

        public void RemoveMax(int value)
        {
            _maxValue -= value;
        }

        public void Add(float value)
        {
            _currentValue = Mathf.Min(_currentValue + value, _maxValue);
        }

        public void Remove(float value)
        {
            _currentValue = Mathf.Max(_currentValue - value, 0);
        }

        public void HoldBonusFor(BonusType bonusType, int amount, float timeInSec)
        {
            float bonusAmount = CalculateBonus(bonusType, amount);

            Add(bonusAmount);

            StartFillRadialImageOverTime(timeInSec);

            GameManager.Instance.StartRemoveBonusAfter(Name, timeInSec, bonusAmount);
        }

        public void ApplyBonusOverTime(BonusType bonusType, float amount, float timeInSec)
        {
            float bonusAmount = CalculateBonus(bonusType, amount);
            float timeStepSec = 0.1f;
            int numberOfSteps = (int)(timeInSec / timeStepSec);
            float bonusStep = bonusAmount / numberOfSteps;

            StartFillRadialImageOverTime(timeInSec/10);

            GameManager.Instance.StartBonusOverTime(Name, bonusStep, numberOfSteps);
        }

        public void RampUpAndHoldFor(BonusType bonusType, float amount, float rampTimeInSec, float holdTimeInSec)
        {
            float bonusAmount = CalculateBonus(bonusType, amount);

            float timeStepSec = 0.1f;
            int numberOfSteps = (int)(rampTimeInSec / timeStepSec);
            float bonusStep = bonusAmount / numberOfSteps;

            StartFillRadialImageOverTime(rampTimeInSec + holdTimeInSec);

            GameManager.Instance.StartBonusOverTime(Name, bonusStep, numberOfSteps);

            GameManager.Instance.StartRemoveBonusAfter(Name, rampTimeInSec + holdTimeInSec, bonusAmount);

        }

        void StartFillRadialImageOverTime(float time)
        {

            GameManager.Instance.StartFillRadialImageOverTime(Name, time);

        }

        float CalculateBonus(BonusType bonusType, float amount)
        {
            if (bonusType.Equals(BonusType.Amount))
            {
                return amount;
            }
            else
            {
                return _currentValue * (amount / 100);
            }
        }

    }
}
