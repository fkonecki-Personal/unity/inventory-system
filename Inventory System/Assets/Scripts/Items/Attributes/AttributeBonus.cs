﻿using UnityEngine;

namespace InventorySystem.Items.Attributes
{
    [System.Serializable]
    public class AttributeBonus
    {
        [SerializeField] AttributeName _attributeName;
        [SerializeField] BonusType _type;
        [SerializeField] BonusApplyType _applyType;
        [SerializeField] float _value;

        public AttributeName AttributeName
        {
            get => _attributeName;
            set => _attributeName = value;
        }
        public BonusType Type
        {
            get => _type;
            set => _type = value;
        }
        public BonusApplyType ApplyType
        {
            get => _applyType;
            set => _applyType = value;
        }
        public float Value
        {
            get => _value;
            set => _value = value;
        }

        public AttributeBonus(AttributeName attributeName,
                              BonusType type = BonusType.Amount,
                              BonusApplyType applyType = BonusApplyType.Default,
                              float value = 0)
        {
            _attributeName = attributeName;
            _type = type;
            _value = value;
            _applyType = applyType;
 
        }
    }
}
