﻿namespace InventorySystem.Items.Attributes
{
    [System.Serializable]
    public enum BonusApplyType
    {
        Default,
        DefaultMax,
        ApplyOverTime,
        Hold,
        RampUpAndHold
    }
}
