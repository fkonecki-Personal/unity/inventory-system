﻿namespace InventorySystem.Items.Inventory
{
    [System.Serializable]
    public class InventorySlot
    {
        ItemObject _item;
        int _amount;
        float _condition;

        public ItemObject Item { get => _item; }
        public int Amount { get => _amount; }
        public float Condition { get => _condition; }

        public InventorySlot(ItemObject item, int amount, float condition)
        {
            _item = item;
            _amount = amount;
            _condition = condition;
        }

        public void AddAmount(int value)
        {
            _amount += value;
        }

        public void RemoveAmount(int value)
        {
            _amount -= value;
        }
    }
}
