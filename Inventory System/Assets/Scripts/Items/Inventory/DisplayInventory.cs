﻿using UnityEngine;

namespace InventorySystem.Items.Inventory
{
    public class DisplayInventory : MonoBehaviour
    {
        [SerializeField] SOInventory _inventory;
        [SerializeField] GameObject _emptySlot;
        [SerializeField] GameObject _screen;

        bool _hasStarted = false;

        void Start()
        {
            _inventory.Clear();
            _screen.SetActive(false);
            _hasStarted = true;
        }

        void OnEnable()
        {
            UpdateDisplay();
            if (_hasStarted)
                GameManager.Instance.AnalyticsScript.OnWindowOpenedAnalytics("InventoryWindow");
        }

        void OnDisable()
        {
            ItemTooltip.Instance.StopDragging();
        }

        public void UpdateDisplay()
        {
            int inventorySize = _inventory.GetCount();

            foreach (Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            int rowsNeeded = inventorySize > 32 ? 1 + (inventorySize - 1) / 8 : 4;

            RectTransform rectTransform = GetComponent<RectTransform>();
            Vector2 sizeDelta = rectTransform.sizeDelta; 
            sizeDelta.y = 290 + 55 * Mathf.Max(0, rowsNeeded - 5);
            rectTransform.sizeDelta = sizeDelta;

            for (int i = 0; i < rowsNeeded * 8; i++)
            {
                var obj = Instantiate(_emptySlot,
                                      Vector3.zero,
                                      Quaternion.identity,
                                      transform);

                ItemSlot itemSlotScript = obj.GetComponent<ItemSlot>();
                UnityEngine.UI.Text slotText = obj.GetComponentInChildren<UnityEngine.UI.Text>();

                slotText.text = "";
                itemSlotScript.SlotNumber = i;
                itemSlotScript.Type = SlotType.Inventory;

                if (inventorySize > i)
                {
                    UnityEngine.UI.Image slotImage = obj.transform.GetChild(1).gameObject.GetComponent<UnityEngine.UI.Image>();
                    Sprite sprite = _inventory.GetAt(i).Item.Prefab.GetComponent<SpriteRenderer>().sprite;

                    slotText.text = _inventory.GetAt(i).Amount.ToString();
                    slotImage.sprite = sprite;
                    slotImage.color = Color.white;
                }
            }
        }
    }
}