﻿using UnityEngine;

namespace InventorySystem.Items.Inventory
{
    public class InventorySort : MonoBehaviour
    {
        [SerializeField] SOInventory _inventory;

        public void Sort()
        {
            _inventory.Sort();
            DisplayUpdater.Instance.UpdateInventoryDisplay();
        }
    }
}