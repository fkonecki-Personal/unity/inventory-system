﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace InventorySystem.Items.Inventory
{
	public class SplitterSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
	{
		[SerializeField] SOInventory _inventory;

		[SerializeField] InputField _inputFieldLeft;
		[SerializeField] InputField _inputFieldRight;

		[SerializeField] GameObject _splitter;

		InventorySlot _slot;

		Color _highlightColor = new Color(0, 1, 0, 0.5f);
		Color _initialColor;

		int _leftAmount;
		int _rightAmount;

		void Start()
		{
			_initialColor = transform.GetComponentInParent<Image>().color;
		}

		public void AddFromInventoryToSplitter(int inventorySlotNumber)
		{
			InventorySlot inventorySlot = _inventory.GetAt(inventorySlotNumber);

			if (inventorySlot.Item.Type.Equals(ItemType.Limited) ||
				inventorySlot.Item.Type.Equals(ItemType.Unlimited))
			{
				_slot = inventorySlot;
				_leftAmount = _slot.Amount;
				_rightAmount = 0;

				_inventory.RemoveAllItemsAt(inventorySlotNumber);

				ItemTooltip.Instance.StopDragging();
				UpdateSplitter();
				UpdateScreens();
			}

		}

		void UpdateScreens()
		{
			DisplayUpdater.Instance.UpdateInventoryDisplay();
		}

		void UpdateSplitter()
		{
			Image image = GetComponentInChildren<Image>();

			if (_slot != null)
			{
				Sprite sprite = _slot.Item.Prefab.GetComponent<SpriteRenderer>().sprite;
				GetComponentInChildren<Text>().text = _slot.Item.ItemName;

				image.sprite = sprite;
				image.color = Color.white;

				UpdateInputFields();

			}
			else
			{
				GetComponentInChildren<Text>().text = "";

				image.sprite = null;
				image.color = Color.clear;
			}
		}

		public bool IsSlotEmpty()
		{
			return _slot == null;
		}

		void Reset()
		{
			_slot = null;
			_leftAmount = 0;
			_rightAmount = 0;
		}

// Pointer actions ****************************************************

		public void OnPointerClick(PointerEventData eventData)
		{
			if (ItemTooltip.Instance.IsItemBeingDragged &&
				ItemTooltip.Instance.ItemSlotType.Equals(SlotType.Inventory) &&
				IsSlotEmpty())
			{
				int inAirSlotNumber = ItemTooltip.Instance.ItemSlotNumber;
				InventorySlot inventorySlot = _inventory.GetAt(inAirSlotNumber);

				if (inventorySlot.Item.Type.Equals(ItemType.Limited) ||
					inventorySlot.Item.Type.Equals(ItemType.Unlimited))
				{
					_slot = inventorySlot;
					AddFromInventoryToSplitter(inAirSlotNumber);
				}
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			transform.GetComponentInParent<Image>().color = _highlightColor;

			if (!IsSlotEmpty())
			{
				ItemTooltip.Instance.ShowTooltip(_slot.Item, _slot.Condition);
			}
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			transform.GetComponentInParent<Image>().color = _initialColor;
			ItemTooltip.Instance.HideTooltip();
		}

// UI actions ******************************************************

		public void LeftInputFieldEdited(string text)
		{ InputFieldEdited(text, true); }

		public void RightInputFieldEdited(string text)
		{ InputFieldEdited(text, false); }

		public void InputFieldEdited(string text, bool isLeft)
		{
			int total = _leftAmount + _rightAmount;
			int i = 0;
			bool result = int.TryParse(text, out i);
			if (result && i <= total && i >= 0)
			{
				_leftAmount = isLeft ? i : total - i;
				_rightAmount = total - _leftAmount;
			}
			UpdateSplitter();
		}

		public void LeftButtonClick()
		{
			if (!ItemTooltip.Instance.IsItemBeingDragged && _rightAmount > 0)
			{
				_leftAmount++;
				_rightAmount--;

				UpdateInputFields();
			}
		}

		public void RightButtonClick()
		{
			if (!ItemTooltip.Instance.IsItemBeingDragged && _leftAmount > 0)
			{
				_rightAmount++;
				_leftAmount--;

				UpdateInputFields();
			}
		}

		void UpdateInputFields()
		{
			_inputFieldLeft.text = _leftAmount.ToString();
			_inputFieldRight.text = _rightAmount.ToString();
		}

		public void OkButtonClick()
		{
			if (!ItemTooltip.Instance.IsItemBeingDragged)
			{ 
				if (_leftAmount == 0 || _rightAmount == 0)
					CancelButtonClick();
				else
				{
					_inventory.AddItemWithoutStacking(_slot.Item, _leftAmount, 100);
					_inventory.AddItemWithoutStacking(_slot.Item, _rightAmount, 100);
				}

				Reset();
				UpdateScreens();
				UpdateSplitter();
				_splitter.SetActive(false);
			}
		}

		public void CancelButtonClick()
		{
			if (!ItemTooltip.Instance.IsItemBeingDragged)
			{
				_inventory.AddItemWithoutStacking(_slot.Item, _leftAmount + _rightAmount, 100);

				Reset();
				UpdateScreens();
				UpdateSplitter();
				_splitter.SetActive(false);
			}
		}

	}
}