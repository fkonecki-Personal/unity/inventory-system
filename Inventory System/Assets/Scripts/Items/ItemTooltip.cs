﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;

using InventorySystem.Items.Inventory;

namespace InventorySystem.Items
{
	public class ItemTooltip : MonoBehaviour
	{
		static ItemTooltip _instance;
		bool _isItemBeingDragged = false;
		int _itemSlotNumber;
		SlotType _itemSlotType;

		ItemObject _displayedItem;
		public GameObject DraggedImage;

		[SerializeField] public GameObject _dropZone;
		[SerializeField] public GameObject _splitter;
		[SerializeField] public GameObject SplitterSlot;

		[SerializeField] Text _nameText;
		[SerializeField] Text _typeText;
		[SerializeField] Text _statsText;

		StringBuilder _stringBuilder = new StringBuilder();

		public static ItemTooltip Instance
		{
			get => _instance;
			set => _instance = value;
		}
		public bool IsItemBeingDragged
		{
			get => _isItemBeingDragged;
			set => _isItemBeingDragged = value;
		}
		public int ItemSlotNumber
		{
			get => _itemSlotNumber;
			set => _itemSlotNumber = value;
		}
		public SlotType ItemSlotType
		{
			get => _itemSlotType;
			set => _itemSlotType = value;
		}

		public ItemObject DisplayedItem
		{
			get => _displayedItem;
		}

		void Awake()
		{
			if (Instance == null)
				Instance = this;
			else
				Destroy(this);

			gameObject.SetActive(false);
		}

		public void ShowTooltip(ItemObject item, float condition, SlotType slotType = SlotType.Inventory)
		{
			if (item)
			{
				_displayedItem = item;
				_itemSlotType = slotType;

				gameObject.SetActive(true);
				UpdateText(condition);
			}
		}

		public void UpdateText(float condition) 
		{
			_nameText.text = _displayedItem.name;
			_typeText.text = _displayedItem.Type.ToString();

			_stringBuilder.Length = 0;

			foreach (var bonus in _displayedItem.Bonuses)
				AddStatText(bonus.Value, " " + bonus.AttributeName);

			if (_displayedItem.Type.Equals(ItemType.Equipable))
				AddConditionText(condition);

			_statsText.text = _stringBuilder.ToString();
		}

		public void HideTooltip()
		{
			if (!IsItemBeingDragged)
			{
				gameObject.SetActive(false);
			}
		}

		void AddStatText(float statBonus, string statName)
		{
			if (statBonus != 0)
			{
				if (_stringBuilder.Length > 0)
					_stringBuilder.AppendLine();

				if (statBonus > 0)
					_stringBuilder.Append("+");

				_stringBuilder.Append(statBonus);
				_stringBuilder.Append(statName);
			}
		}

		void AddConditionText(float condition)
		{
			if (_stringBuilder.Length > 0)
				_stringBuilder.AppendLine();

			_stringBuilder.Append("Condition ");
			_stringBuilder.Append((int)condition);
			_stringBuilder.Append("%");
		}


		public void StartDragging(SlotType type, int slotNumber)
		{
			IsItemBeingDragged = true;
			ItemSlotType = type;
			ItemSlotNumber = slotNumber;
			_dropZone.SetActive(true);
			_splitter.SetActive(true);
		}

		public void StopDragging()
		{
			IsItemBeingDragged = false;
			HideTooltip();
			if (_dropZone) _dropZone.SetActive(false);

			if (SplitterSlot.GetComponent<SplitterSlot>().IsSlotEmpty())
				_splitter.SetActive(false);
		}

	}
}