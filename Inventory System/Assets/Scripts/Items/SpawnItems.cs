﻿using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem.Items
{
    public class SpawnItems : MonoBehaviour
    {
        [SerializeField] GameObject _player;
        [SerializeField] List<ItemObject> _items;

        Transform _parentTransform;

        void Start()
        {
            _player = GameObject.Find("Player");
            _parentTransform = GameObject.Find("Items").transform;
        }

        public void Spawn()
        {
            Instantiate(_items[Random.Range(0, 9)].Prefab.gameObject,
                        RandomPositionInMinMaxPlayerRadius(0.8f, 1f),
                        Quaternion.identity,
                        _parentTransform);
        }

        Vector3 RandomPositionInMinMaxPlayerRadius(float min, float max)
        {
            Vector3 playerPosition = _player.transform.position;
            playerPosition.z = 5;
            int angle = Random.Range(0, 360);

            return playerPosition +
                (
                    new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad),
                                Mathf.Sin(angle * Mathf.Deg2Rad),
                                0) *
                    Random.Range(min, max)
                );
        }
    }
}