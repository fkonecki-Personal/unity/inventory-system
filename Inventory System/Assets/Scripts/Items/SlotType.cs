﻿namespace InventorySystem.Items
{
	[System.Serializable]
	public enum SlotType
	{
		Inventory,
		Equipment
	};
}