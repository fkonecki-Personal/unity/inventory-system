﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace InventorySystem.Items
{
	public class Drag : MonoBehaviour, IPointerClickHandler
	{
		Canvas _canvas;

		GameObject _originalParent;
		ItemSlot _parentSlotScript;

		bool _isHolding = false;

		void Start()
		{
			_canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
			_originalParent = gameObject.transform.parent.gameObject;
			_parentSlotScript = gameObject.GetComponentInParent<ItemSlot>();
		}

		void Update()
		{
			if (_isHolding)
			{
				DragHandler(Input.mousePosition + new Vector3(0, 17, 0));

				if (!ItemTooltip.Instance.IsItemBeingDragged)
				{
					ReturnItem();
				}
			}
		}

		public void OnLongPress()
		{
			if (_parentSlotScript.IsBeingViewed && 
				!_parentSlotScript.IsSlotEmpty() &&
				!_isHolding &&
				!ItemTooltip.Instance.IsItemBeingDragged)
			{

				_isHolding = true;
				ItemTooltip.Instance.StartDragging(_originalParent.GetComponent<ItemSlot>().Type,
											_originalParent.GetComponent<ItemSlot>().SlotNumber);

				gameObject.transform.SetParent(_canvas.transform);
				DragHandler(transform.position);

				_parentSlotScript.IsBeingViewed = false;
			}
		}


		public void OnPointerClick(PointerEventData eventData)
		{
			if (!GameManager.Instance.IsTouchEnabled)
			{
				if (eventData.button == PointerEventData.InputButton.Left &&
					!_parentSlotScript.IsSlotEmpty() &&
					!_isHolding &&
					!ItemTooltip.Instance.IsItemBeingDragged)
				{
					ItemTooltip.Instance.DraggedImage = gameObject;

					_isHolding = true;
					ItemTooltip.Instance.StartDragging(_originalParent.GetComponent<ItemSlot>().Type,
												_originalParent.GetComponent<ItemSlot>().SlotNumber);

					gameObject.transform.SetParent(_canvas.transform);

					DragHandler(eventData.position);
				}
				else
				{
					_parentSlotScript.OnPointerClick(eventData);
				}
			}
		}

		public void ReturnItem()
		{
			_isHolding = false;
			gameObject.transform.SetParent(_originalParent.transform);
			gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
		}

		public void DragHandler(Vector3 position)
		{
			Vector2 outPosition;

			RectTransformUtility.ScreenPointToLocalPointInRectangle(
					(RectTransform)_canvas.transform,
					position,
					_canvas.worldCamera,
					out outPosition);

			transform.position = _canvas.transform.TransformPoint(outPosition);
		}
	}
}