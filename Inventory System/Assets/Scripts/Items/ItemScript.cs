﻿using UnityEngine;

namespace InventorySystem.Items
{
    public class ItemScript : MonoBehaviour
    {
        [SerializeField] ItemObject _item;
        [SerializeField] float _condition = 100;

        public ItemObject Item
        {
            get => _item;
            set => _item = value;
        }
        public float Condition
        {
            get => _condition;
            set => _condition = value;
        }
    }
}
