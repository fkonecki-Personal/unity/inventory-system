﻿using UnityEngine;
using System.Collections.Generic;

using InventorySystem.Items;
using InventorySystem.Items.Attributes;

namespace InventorySystem
{
    [System.Serializable]
    public class GameManager : MonoBehaviour
    {
        [SerializeField] SOAttributes _attributes;
        [SerializeField] GameEvent _enableInput;
        [SerializeField] GameEvent _disableInput;
        [SerializeField] public List<GameObject> _attributeRadialImages;

        AnalyticsScript _analyticsScript;
        TimedAttributeUpdateScript _coroutinesScript;
        TouchInput _touchInput;

        static GameManager _instance;
        bool _isInputEnabled = true;
        bool _isTouchEnabled = false;
        int _lastViewedInventorySlot;

// Get/Set **********************************
        public static GameManager Instance
        {
            get => _instance;
            set => _instance = value;
        }

        public bool IsInputEnabled
        {
            get => _isInputEnabled;
            set => _isInputEnabled = value;
        }

        public AnalyticsScript AnalyticsScript { get => _analyticsScript; }

        public bool IsTouchEnabled 
        { 
            get => _isTouchEnabled; 
            set => _isTouchEnabled = value;
        }
        public int LastViewedInventorySlot
        {
            get => _lastViewedInventorySlot;
            set => _lastViewedInventorySlot = value;
        }

// Mono *****************************************

        void Awake()
        {
            // Application.platform == RuntimePlatform.Android;
            IsTouchEnabled = false;

            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        void Start()
        {
            _analyticsScript = GetComponent<AnalyticsScript>();
            _coroutinesScript = GetComponent<TimedAttributeUpdateScript>();

            if (IsTouchEnabled) 
                _touchInput = GetComponent<TouchInput>();

            _analyticsScript.OnStartupAnalytics();
        }

// Input *********************************

        public void EnableInput()
        {
            _enableInput.TriggerEvent();
            _isInputEnabled = true;
        }

        public void DisableInput()
        {
            _disableInput.TriggerEvent();
            _isInputEnabled = false;
        }

 //  *******************************

        public void UpdateAllDisplays()
        {
            DisplayUpdater.Instance.UpdateAllDisplays();
        }

        public void RemoveBonuses(EquipableObject equipableObject)
        {
            _attributes.RemoveBonuses(equipableObject);
        }
        
// Coroutines ********************************

        public void StartBonusOverTime(AttributeName name, float bonusStep, int numberOfSteps)
        {
            StartCoroutine(
                _coroutinesScript.BonusOverTimeCoroutine(name, bonusStep, numberOfSteps));
        }

        public void StartRemoveBonusAfter(AttributeName name, float waitTimeSec, float bonusAmount)
        {
            StartCoroutine(
                _coroutinesScript.RemoveBonusAfter(name, waitTimeSec, bonusAmount));
        }

        public void StartFillRadialImageOverTime(AttributeName name, float time)
        {
            UnityEngine.UI.Image image =
                _attributeRadialImages[(int)name].GetComponent<UnityEngine.UI.Image>();
            image.fillAmount = 0;
            float waitTime = time / 10;

            StartCoroutine(
                _coroutinesScript.FillRadialImageOverTime(image, waitTime));
        }
    }

}