﻿using System.Collections;
using UnityEngine;

using InventorySystem.Items.Attributes;

namespace InventorySystem
{
    public class TimedAttributeUpdateScript : MonoBehaviour
    {

        [SerializeField] SOAttributes _attributes;

        public IEnumerator FillRadialImageOverTime(UnityEngine.UI.Image image, float waitTimeSec)
        {
            for (int step = 0; step < 10; step++)
            {
                image.fillAmount += 0.1f;
                Color color = new Color(1 - image.fillAmount, 1, 1 - image.fillAmount, 1);
                image.color = color;

                yield return new WaitForSeconds(waitTimeSec);
            }
            image.fillAmount = 0;
            image.color = Color.white;
        }


        public IEnumerator BonusOverTimeCoroutine(AttributeName name, float bonusStep, int numberOfSteps)
        {
            for (int step = 0; step <= numberOfSteps; step++)
            {
                _attributes.Add(name, bonusStep);
                DisplayUpdater.Instance.UpdateAttributesDisplay();

                yield return null;
            }
        }

        public IEnumerator RemoveBonusAfter(AttributeName name, float waitTimeSec, float bonusAmount)
        {
            yield return new WaitForSeconds(waitTimeSec);

            _attributes.Remove(name, bonusAmount);
            DisplayUpdater.Instance.UpdateAttributesDisplay();
        }
    }
}
